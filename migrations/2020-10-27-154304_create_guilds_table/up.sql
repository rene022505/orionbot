-- Your SQL goes here
CREATE TABLE guilds (
  guild_id text PRIMARY KEY NOT NULL,
  richest_role text,
  longest_spin_role text,
  invite_log_channel text
);