-- Your SQL goes here
CREATE TABLE guild_command_config (
  guild_id text NOT NULL,
  command_name text NOT NULL,
  is_whitelist smallint NOT NULL,
  PRIMARY KEY(guild_id, command_name)
);