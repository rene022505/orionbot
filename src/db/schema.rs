table! {
    guild_channel_config (guild_id, channel_id, command_name) {
        guild_id -> Text,
        channel_id -> Text,
        command_name -> Text,
    }
}

table! {
    guild_command_config (guild_id, command_name) {
        guild_id -> Text,
        command_name -> Text,
        is_whitelist -> Int2,
    }
}

table! {
    guilds (guild_id) {
        guild_id -> Text,
        richest_role -> Nullable<Text>,
        longest_spin_role -> Nullable<Text>,
        invite_log_channel -> Nullable<Text>,
    }
}

table! {
    invites (code) {
        code -> Text,
        inviter -> Text,
        uses -> Int8,
        guild_id -> Text,
        max_age -> Int8,
        max_uses -> Int8,
        created_at -> Timestamptz,
        temporary -> Bool,
    }
}

table! {
    spinners (discord_id, guild_id) {
        discord_id -> Text,
        channel_id -> Text,
        knocked_user_id -> Text,
        duration -> Int8,
        guild_id -> Text,
    }
}

table! {
    users (discord_id, guild_id) {
        discord_id -> Text,
        nuggets -> Int8,
        last_mine -> Timestamptz,
        last_hourly -> Timestamptz,
        has_pickaxe -> Bool,
        last_spin -> Timestamptz,
        guild_id -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    guild_channel_config,
    guild_command_config,
    guilds,
    invites,
    spinners,
    users,
);
