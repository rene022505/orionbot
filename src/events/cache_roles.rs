use serenity:: {
    cache::Cache,
    http::client::Http,
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use std::sync::Arc;

use crate::db::*;
use crate::cache::*;

pub async fn cache_roles(http: Arc<Http>, cache: Arc<Cache>, db: Arc<Db>, bot_cache: Arc<Mutex<BotCache>>) {

    // TODO: Fix this

    /*for guild_id in cache.guilds().await {

        let guild_id_u64 = *guild_id.as_u64();

        let db_guild = match db.get_guild(guild_id_u64).await {
            Ok(optn_guild) => match optn_guild {
                Some(g) => g,
                None => return,
            }
            Err(e) => {
                error!("Database error get guild: {:?}", e);
                return;
            }
        };

        if db_guild.richest_role.0 != 0 {
            let nuggets = match db.select_top_10_users(guild_id_u64).await {
                Ok(users) => users,
                Err(e) => {
                    error!("Database error select top 10 users: {:?}", e);
                    return;
                }
            };

            if let Some(u) = nuggets.first() {
                bot_cache.lock().await.richest.insert(guild_id_u64, u.discord_id.0);
        
                let mut member = guild_id.member(&http, &UserId(u.discord_id.0)).await.unwrap();
        
                match member.add_role(&http, db_guild.richest_role.0).await {
                    Ok(_) => {},
                    Err(e) => {
                        error!("Failed to add Richest role for {}: {:?}", u.discord_id.0, e);
                    }
                };
            }
        }
    
        if db_guild.longest_spin_role.0 != 0 {
            let spinners = match db.select_top_10_spinners(guild_id_u64).await {
                Ok(users) => users,
                Err(e) => {
                    error!("Database error select top 10 spinners: {:?}", e);
                    return;
                }
            };
        
            if let Some(u) = spinners.first() {
                bot_cache.lock().await.longest_spin.insert(guild_id_u64, u.discord_id.0);
        
                let mut member = guild_id.member(&http, &UserId(u.discord_id.0)).await.unwrap();
        
                match member.add_role(&http, db_guild.longest_spin_role.0).await {
                    Ok(_) => {},
                    Err(e) => {
                        error!("Failed to add Longest Spin role for {}: {:?}", u.discord_id.0, e);
                    }
                };
            }
        }        
    }*/
}