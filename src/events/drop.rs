use serenity:: {
    framework::standard::macros::hook,

    collector::MessageCollectorBuilder,
    futures::stream::StreamExt,

    model::channel::Message
};

use rand::prelude::*;

use crate::cache::*;
use crate::db::*;

use chrono::prelude::*;
use chrono::Duration;

use serenity::prelude::*;
use serenity::utils::Colour;

use diesel::SaveChangesDsl;

const MIN_RATE: f32 = 7.5; // Messages per Minute

const MIN_TIME: i64 = 5; // Minutes

const CLAIM_TIME: u64 = 10; // Seconds

const ANTI_SPAM_DELAY: i64 = 3; // Seconds

const MAX_DROP_AMOUNT: i64 = 100; // Nuggets

const DROP_COOLDOWN: i64 = 60; // Minutes

const DROP_DELAY: u64 = 20; // Seconds

#[hook]
pub async fn on_message(ctx: &Context, msg: &Message) {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let mut random = rand::rngs::OsRng;

    if msg.is_private() {
        return;
    }

    let channel_id = *msg.channel_id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    if msg.author.bot {
        return;
    }

    match database.is_command_enabled(guild_id, channel_id, "drop").await {
        Ok(b) => if !b {
            return;
        },
        Err(e) => {
            error!("Random Nugget Drop Database Error: {:?}", e);
            return;
        }
    }

    let time_difference;
    let new_messages;

    {
        let mut lock = bot_cache.lock().await;

        if let Some(is_dropping) = lock.loot_dropping.get(&guild_id) {
            if *is_dropping {
                return;
            }
        }

        if let Some(cooldown) = lock.drop_cooldown.get(&guild_id) {
            if Utc::now() < *cooldown {
                let keys: Vec<u64> = lock.nugget_drop.iter()
                    .filter_map(|(k, v)| if v.guild_id == guild_id { Some(*k) } else { None }).collect();

                for key in keys
                {
                    lock.nugget_drop.remove(&key);
                }

                return;
            }
        }

        match lock.nugget_drop.get_mut(&channel_id) {
            Some(drop) => {

                if Utc::now() - drop.time_last_message < Duration::seconds(ANTI_SPAM_DELAY) {
                    return;
                }

                drop.messages += 1;
                drop.time_last_message = Utc::now();

                new_messages = drop.messages;
                time_difference = Utc::now() - drop.time_start;
            },
            None => {
                lock.nugget_drop.insert(channel_id, 
                    NuggetDrop {
                        guild_id,
                        time_start: Utc::now(),
                        time_last_message: Utc::now(),
                        messages: 1,
                    });

                return;
            }
        }
    }

    if time_difference < Duration::minutes(MIN_TIME) {
        return;
    }

    let rate = new_messages as f32 / time_difference.num_minutes() as f32;

    if rate < MIN_RATE {

        let mut lock = bot_cache.lock().await;

        lock.nugget_drop.remove(&channel_id);

        //debug!("Drop rate: {}; Channel ID: {}; Messages: {}; Minutes: {}", rate, channel_id, new_messages, time_difference.num_minutes());

        return;
    }

    {
        let mut lock = bot_cache.lock().await;

        lock.nugget_drop.remove(&channel_id);

        lock.loot_dropping.insert(guild_id, true);
        lock.drop_cooldown.insert(guild_id, Utc::now() + Duration::minutes(DROP_COOLDOWN));
    }

    let nuggets = ((20.0 + random.gen::<f32>() * (0.5 * rate).exp()) as i64).min(MAX_DROP_AMOUNT);

    tokio::time::sleep(tokio::time::Duration::from_secs(DROP_DELAY)).await;

    let mut claim_message = match msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Random Nuggets Drop! 💎")
            .description(format!("Quick! Claim {} Nuggets! :gem:, by replying with `claim`!",
                nuggets))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await {
        Ok(m) => m,
        Err(e) => {
            error!("Random Nugget Drop Message -- {}", e);
            return;
        }
    };

    let mut collector = MessageCollectorBuilder::new(&ctx)
        .channel_id(msg.channel_id)
        .timeout(std::time::Duration::from_secs(CLAIM_TIME))
        .filter(|message| 
            !message.author.bot &&
            message.content.trim().to_lowercase().starts_with("claim")
        )
        .await;

    let message = match collector.next().await{
        Some(m) => m,
        None => {
            match claim_message.edit(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Random Nuggets Drop! 💎")
                    .description(format!("~~Quick! Claim {} Nuggets! :gem:, by replying with `claim`!~~ Time's up!",
                        nuggets))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await {
                Ok(_) => {},
                Err(e) => {
                    error!("Random Nugget Drop Edit Message -- {}", e);
                    return;
                }
            }

            let mut lock = bot_cache.lock().await;

            lock.loot_dropping.insert(guild_id, false);

            return;
        }
    };

    let user_id = *message.author.id.as_u64();

    let total_nuggets = match database.get_user(user_id, guild_id).await {
        Ok(Some(u)) => {
            let mut update = u.update();
            update.nuggets = Some(u.nuggets + nuggets);

            let _: DbUser = match update.save_changes(&*database.conn.lock().await) {
                Ok(u) => u,
                Err(e) => {
                    error!("Update User Random Loot Drop -- {}", e);
                    return;
                }
            };

            u.nuggets + nuggets
        },
        Ok(None) => {
            let mut default = DbUser::default(user_id, guild_id);

            default.nuggets += nuggets;

            match database.add_user(default.clone()).await {
                Ok(_) => {},
                Err(e) => {
                    error!("Add Default User Random Loot Drop -- {}", e);
                    return;
                }
            }

            default.nuggets + nuggets
        },

        Err(e) => {
            error!("Get User Random Loot Drop -- {}", e);
            return;
        }
    };

    match msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Random Nuggets Drop! 💎")
            .description(format!("Success! {} has claimed {} Nuggets! :gem: They now have {} Nuggets! :gem:",
                message.author.mention(), nuggets, total_nuggets))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await {
        Ok(_) => {},
        Err(e) => {
            error!("Random Nugget Drop Success Message -- {}", e);
            return;
        }
    }

    let mut lock = bot_cache.lock().await;

    lock.loot_dropping.insert(guild_id, false);
}