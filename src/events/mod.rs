use serenity:: {
    async_trait,

    client::bridge::gateway::event::ShardStageUpdateEvent,
    gateway::ConnectionStage,

    model::{
        gateway::Ready,
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

mod drop;
pub use drop::*;

mod cache_roles;
mod invite;

use crate::cache::*;

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);

        let data = ctx.data.read().await;
        let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
        let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

        cache_roles::cache_roles(ctx.http.clone(), ctx.cache.clone(), database.clone(), bot_cache.clone()).await;
        invite::cache_invites(ctx.http.clone(), ctx.cache.clone(), database.clone()).await;
    }

    async fn resume(&self, _: Context, resume: ResumedEvent) {
        info!("Resumed! Trace: {:?}",  resume.trace);
    }

    async fn shard_stage_update(&self, _ctx: Context, update: ShardStageUpdateEvent) {
        
        info!("Shard {} update event: Old: {:?}; New: {:?}", update.shard_id, update.old, update.new);

        if let ConnectionStage::Connecting = update.new {
            warn!("Shard {} has disconnected! Reconnecting...", update.shard_id);
        }

        if let ConnectionStage::Resuming = update.new {
            warn!("Resuming Shard {}...", update.shard_id);
        }
    }

    async fn invite_create(&self, ctx: Context, event: InviteCreateEvent) {
        invite::invite_create(ctx, event).await;
    }

    async fn invite_delete(&self, ctx: Context, event: InviteDeleteEvent) {
        invite::invite_delete(ctx, event).await;
    }

    async fn guild_member_addition(&self, ctx: Context, guild_id: GuildId, member: Member) {
        invite::join(ctx, guild_id, member).await;
    }
}

