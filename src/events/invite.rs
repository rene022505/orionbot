use serenity:: {
    cache::Cache,
    http::client::Http,
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use chrono::prelude::*;

use tokio::time::{ sleep, Duration };

use crate::cache::*;
use crate::db::*;

use nebbot_utils::time::human_readable;
use nebbot_utils::types::*;

use diesel::SaveChangesDsl;

use std::sync::Arc;

pub async fn invite_create(ctx: Context, event: InviteCreateEvent) {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    
    let invite = DbInvite {
        code: event.code,
        inviter: TextU64(event.inviter.unwrap().id.0),
        guild_id: TextU64(event.guild_id.unwrap().0),
        uses: 0,
        max_age: event.max_age as i64,
        max_uses: event.max_uses as i64,
        created_at: Utc::now(),
        temporary: event.temporary,
    };

    let log_channel = database.get_guild(event.guild_id.unwrap().0).await.expect("Failed to retrieve guild from database.")
        .map(|x| x.invite_log_channel).flatten().map(|x| ChannelId(x.0));

    if let Some(channel) = log_channel {
        let user = UserId(invite.inviter.0).to_user(&ctx.http).await.expect("Could not retrieve user from REST API.");

        channel.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Invite Created")
                .description(format!("https://discord.gg/{}", invite.code))
                .thumbnail(user.avatar_url().unwrap_or(user.default_avatar_url()))
                .field("Invite Creator", user.mention(), true)
                .field("Invite Max Uses", if invite.max_uses > 0 { invite.max_uses.to_string() } else { "∞".to_owned() }, true)
                .field("Invite Duration", if invite.max_age > 0 { human_readable(chrono::Duration::seconds(invite.max_age)) } else { "∞".to_owned() }, true)
                .timestamp(&invite.created_at)
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await.expect("Failed to send invite create log.");
    }

    database.add_invite(invite.clone()).await.expect("Failed to add invite to the database.");

    let cloned_database = database.clone();

    let cloned_http = ctx.http.clone();

    // TODO: Implement a propper wait thing.
    // Wait until expiry
    if invite.max_age > 0 {
        tokio::task::spawn(async move {
            sleep(Duration::from_secs(invite.max_age as u64)).await;          

            if let Some(channel) = log_channel {
                if let Some(updated_invite) = cloned_database.get_invite(invite.code.clone()).await.expect("Failed to get expired invite.") {
                    let user = UserId(invite.inviter.0).to_user(&cloned_http).await.expect("Could not retrieve user from REST API.");

                    let usage = format!("{}{}", updated_invite.uses, if updated_invite.max_uses > 0 { format!("/{}", updated_invite.max_uses) } else { "".to_owned() });

                    channel.send_message(&cloned_http, |m| {
                        m.embed(|e| {
                            e
                            .title("Invite Expired")
                            .description(format!("https://discord.gg/{}", updated_invite.code))
                            .thumbnail(user.avatar_url().unwrap_or(user.default_avatar_url()))
                            .field("Invite Creator", user.mention(), true)
                            .field("Invite Created at", updated_invite.created_at, true)
                            .field("Invite Usage", usage, true)
                            .timestamp(&Utc::now())
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await.expect("Failed to send invite expire log.");
                }
            }

            cloned_database.remove_invite(invite.code.clone()).await.expect("Failed to expire invite.");
        });
    }
}

pub async fn invite_delete(ctx: Context, event: InviteDeleteEvent) {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    
    let log_channel = database.get_guild(event.guild_id.unwrap().0).await.expect("Failed to retrieve guild from database.")
    .map(|x| x.invite_log_channel).flatten().map(|x| ChannelId(x.0));

    if let Some(channel) = log_channel {
        if let Some(invite) = database.get_invite(event.code.clone()).await.expect("Failed to get invite from the database.") {
            let user = UserId(invite.inviter.0).to_user(&ctx.http).await.expect("Could not retrieve user from REST API.");

            // TODO: fix usage being incorrect somehow, probs waiting for the join thing at the same time
            // for when the invite expires due to usage before the join event is called.
            let usage = format!("{}{}", invite.uses, if invite.max_uses > 0 { format!("/{}", invite.max_uses) } else { "".to_owned() });

            channel.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Invite Deleted")
                    .description(format!("https://discord.gg/{}", invite.code))
                    .thumbnail(user.avatar_url().unwrap_or(user.default_avatar_url()))
                    .field("Invite Creator", user.mention(), true)
                    .field("Invite Created at", invite.created_at, true)
                    .field("Invite Usage", usage, true)
                    .timestamp(&Utc::now())
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await.expect("Failed to send invite delete log.");
        }
    }

    database.remove_invite(event.code).await.expect("Failed to remove invite from the database.");
}

pub async fn join(ctx: Context, guild_id: GuildId, member: Member) {
    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");

    let cached_invites = database.get_all_invites(guild_id.0).await.expect("Failed to get invites from the database.");

    let invites = guild_id.invites(&ctx.http).await.expect("Failed to get invites from guild.");

    // Get cached invite for which the usage count does not match with up to date data.
    let used_invite: DbInvite = match invites.iter().find_map(|x| {
        let cached = cached_invites.iter().find(|c| c.code == x.code).unwrap();
        if cached.uses as u64 != x.uses { Some(cached) }
        else { None }
    }) {
        Some(invite) => {
            let mut i = invite.clone();

            i.uses += 1;

            i.save_changes(&*database.conn.lock().await).expect("Failed to update invite usage.")
        },
        None => {
            // Invite has now expired due to being used completely.
            // Instead, find the one missing invite.
            let mut invite = cached_invites.iter().find_map(|c| 
                match invites.iter().find(|x| x.code == c.code) {
                    Some(_) => None,
                    None => Some(c),
                }
            ).unwrap().clone();

            invite.uses += 1;

            database.remove_invite(invite.code.clone()).await.expect("Failed to expire used invite.");

            invite
        }
    };

    let log_channel = database.get_guild(guild_id.0).await.expect("Failed to retrieve guild from database.")
        .map(|x| x.invite_log_channel).flatten().map(|x| ChannelId(x.0));

    if let Some(channel) = log_channel {
        let user = UserId(used_invite.inviter.0).to_user(&ctx.http).await.expect("Could not retrieve user from REST API.");

        let usage = format!("{}{}", used_invite.uses, if used_invite.max_uses > 0 { format!("/{}", used_invite.max_uses) } else { "".to_owned() });

        channel.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("New User Joined")
                .description(format!("https://discord.gg/{}", used_invite.code))
                .thumbnail(user.avatar_url().unwrap_or(user.default_avatar_url()))
                .field("Invited User", member.mention(), true)
                .field("Invite Creator", user.mention(), true)
                .field("Invite Created at", used_invite.created_at, true)
                .field("Invite Usage", usage, true)
                .timestamp(&Utc::now())
                .colour(Colour::from_rgb(52, 152, 219))
                .image(member.user.avatar_url().unwrap_or(member.user.default_avatar_url()))
            })
        }).await.expect("Failed to send invite delete log.");
    }
}

pub async fn cache_invites(http: Arc<Http>, cache: Arc<Cache>, database: Arc<Db>) {
    info!("Caching invites.");
    for guild_id in cache.guilds().await {
        let guild_id_u64 = guild_id.0;

        database.clear_invites(guild_id_u64).await.expect("Failed to clear invites.");

        let all_invites = match guild_id.invites(&http).await {
            Ok(i) => i,
            Err(SerenityError::Model(ModelError::InvalidPermissions(_))) => continue,
            Err(e) => {
                error!("{:?}", e);
                continue;
            }
        };

        for invite in all_invites.iter() {
            database.add_invite(invite.clone().into()).await.expect("Failed to add invite to the database.");
        }

        info!("Successfully cached {} invites for `{}` ({}).", all_invites.len(),
            guild_id.to_partial_guild(&http).await.expect("Failed to get guild data.").name,
            guild_id_u64);
    }
    info!("Finished caching invites.")
}