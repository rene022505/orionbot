mod blackjack;
pub use blackjack::*;

mod slots;
pub use slots::*;

use serenity::framework::standard::macros::group;

#[group]
#[commands(blackjack, slots)]
struct Gambling;

// TODO: adjust price
pub const SLOT_PRICE: i64 = 40;
