use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use rand::prelude::*;

use crate::db::DbUser;
use crate::cards::*;

use diesel::SaveChangesDsl;

#[command]
#[description("Play Blackjack!")]
#[aliases("bj")]
#[only_in(guilds)]
async fn blackjack(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mut random = rand::rngs::OsRng;

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let host = {
        match database.get_user(discord_id, guild_id).await? {
            Some(u) => u,
            None => {
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Blackjack")
                        .description("You don't have enough Nuggets! :gem:")
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                return Ok(())
            }
        }
    };
    
    let bet_amount = match args.current() {
        Some(b) => match b.parse::<i64>()  {
            Ok(i) => {
                if i < 2 {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Blackjack")
                            .description("Amount out of range.")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;

                    return Ok(());
                }

                i
            },
            Err(_) => {
                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Blackjack")
                        .description("Invalid amount")
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                return Ok(());
            }
        },
        None => {
            let mut bet_message = msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Blackjack")
                    .description("Enter the amount of Nuggets! :gem: you want to bet. (Min 2)")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            match msg.author.await_reply(&ctx).timeout(std::time::Duration::from_secs(30)).await {
                Some(msg) => {
                    match msg.content.trim().split(" ").next() {
                        Some(c) => match c.parse::<i64>()  {
                            Ok(i) => {
                                if i < 2 {
                                    msg.channel_id.send_message(&ctx.http, |m| {
                                        m.embed(|e| {
                                            e
                                            .title("Blackjack")
                                            .description("Amount out of range.")
                                            .colour(Colour::from_rgb(52, 152, 219))
                                        })
                                    }).await?;

                                    return Ok(());
                                }
            
                                i
                            },
                            Err(_) => {
                                msg.channel_id.send_message(&ctx.http, |m| {
                                    m.embed(|e| {
                                        e
                                        .title("Blackjack")
                                        .description("Invalid amount")
                                        .colour(Colour::from_rgb(52, 152, 219))
                                    })
                                }).await?;
            
                                return Ok(());
                            }
                        }
                        None => {
                            msg.channel_id.send_message(&ctx.http, |m| {
                                m.embed(|e| {
                                    e
                                    .title("Blackjack")
                                    .description("Invalid amount")
                                    .colour(Colour::from_rgb(52, 152, 219))
                                })
                            }).await?;
        
                            return Ok(());
                        }
                    }
                }
                None => {
                    bet_message.edit(&ctx.http, |m| {
                        m
                        .embed(|e| {
                            e
                            .title("Blackjack")
                            .description("~~Enter the amount of Nuggets! :gem: you want to bet. (Min 2)~~\n\nTime out.")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
        
                    return Ok(());
                }
            }
        }
    };
    
    if bet_amount > host.nuggets {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Blackjack")
                .description("You don't have enough Nuggets! :gem:")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    let mut update = host.update();

    update.nuggets = Some(host.nuggets - bet_amount);
    let host: DbUser = update.save_changes(&*database.conn.lock().await)?;

    let mut deck = DECK.clone().to_vec();
    deck.append(&mut DECK.clone().to_vec());
    //deck.append(&mut DECK.clone().to_vec()); because @captain_cuckoo#4446 is annoying

    deck.shuffle(&mut random);

    let mut computer = vec![deck.pop().unwrap(), deck.pop().unwrap()];

    let mut player = vec![deck.pop().unwrap(), deck.pop().unwrap()];

    if hand_value(&player) == 21 {
        if hand_value(&computer) == 21 {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Blackjack")
                    .description(format!("Dealer: {} {}\n\nYou: {} {}\n\nIts a tie!", 
                        computer[0], computer[1],
                        player[0], player[1]))
                    .colour(Colour::from_rgb(52, 152, 219))
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                })
            }).await?;

            let mut update = host.update();

            update.nuggets = Some(host.nuggets + bet_amount);

            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

            return Ok(());
        }
        else {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Blackjack")
                    .description(format!("Dealer: {} {}\n\nYou: {} {}\n\nBlackjack! You win!", 
                        computer[0], computer[1],
                        player[0], player[1]))
                    .colour(Colour::from_rgb(52, 152, 219))
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                })
            }).await?;

            let mut update = host.update();

            update.nuggets = Some(host.nuggets + (bet_amount as f32 * 2.50).round() as i64);

            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

            return Ok(());
        }
    };

    // Due to collectors waiting, the host object may now be out of date.
    // Therefore, we drop the object, and store the change in nuggets
    // and retrieve the user and update it again once the game has finished.
    drop(host);
    let mut money_delta = 0;

    let mut text = format!("Dealer: {} [##]\n\nYou: {} {}\n\n
            Hit (Take another card) ⬇️\n
            Stand (Do nothing) ❌", 
        computer[0],
        player[0], player[1]);

    let mut game = msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Blackjack")
            .description(text.clone())
            .colour(Colour::from_rgb(52, 152, 219))
            .footer(|f| f
                .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                .text(msg.author.tag())
            )
        })
    }).await?;

    // ⬇️ ❌💲

    game.react(&ctx.http, ReactionType::Unicode("⬇️".to_string())).await?;
    game.react(&ctx.http, ReactionType::Unicode("❌".to_string())).await?;

    let mut finished = false;

    let mut player_value = hand_value(&player);
    let mut player_hand = String::new();

    for card in player.clone() {
        player_hand.push_str(format!("{} ", card).as_str());
    }

    while !finished {
        match game.await_reaction(&ctx).timeout(std::time::Duration::from_secs(30)).author_id(msg.author.id).filter(
            |reaction| match reaction.emoji.as_data().as_str() {
                "⬇️" | "❌" => true,
                _ => false,
            }
        ).await 
        {
            Some(action) => {
                let reaction = &action.as_inner_ref();

                match reaction.emoji.as_data().as_str() {
                    "⬇️" => {
                        let new = deck.pop().unwrap();

                        player_hand.push_str(format!("{} ", new).as_str());

                        player.push(new);

                        player_value = hand_value(&player);
                
                        //debug!("HIT: New Hand: {}, Value: {}, Vec: {:?}", player_hand, player_value, player.clone());

                        text = format!("Dealer: {} [##]\n\nYou: {}\n\n
                                Hit (Take another card) ⬇️\n
                                Stand (Do nothing) ❌", 
                            computer[0],
                            player_hand.clone());

                        reaction.delete(&ctx.http).await?;
                    }
                    "❌" => {

                        //debug!("STAND: Hand: {}, Value: {}, Vec: {:?}", player_hand, player_value, player.clone());

                        finished = true;

                        reaction.delete(&ctx.http).await?;
                    }

                    _ => {}
                }
            }

            None => {

                //debug!("TIMEOUT: Hand: {}, Value: {}, Vec: {:?}", player_hand, player_value, player.clone());

                finished = true;
            }
        }

        if player_value > 21 {

            //debug!("OVER 21: Hand: {}, Value: {}, Vec: {:?}", player_hand, player_value, player.clone());

            game.edit(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Blackjack")
                    .description(format!("Dealer: {} {}\n\nYou: {}\n\nBusted!", 
                        computer[0], computer[1],
                        player_hand))
                    .colour(Colour::from_rgb(52, 152, 219))
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                })
            }).await?;

            return Ok(());
        }

        if player_value == 21 {

            //debug!("21: Hand: {}, Value: {}, Vec: {:?}", player_hand, player_value, player.clone());

            break;
        }

        if !finished {
            game.edit(&ctx.http, |m| {
                m
                .embed(|e| {
                    e
                    .title("Blackjack")
                    .description(text.clone())
                    .colour(Colour::from_rgb(52, 152, 219))
                    .footer(|f| f
                        .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                        .text(msg.author.tag())
                    )
                })
            }).await?;
        }
    }

    let mut computer_value = hand_value(&computer);

    //debug!("INITIAL COMPUTER: Vec: {:?}, Value: {}", computer.clone(), computer_value);

    while computer_value < 17 {
        let next = deck.pop().unwrap();

        computer.push(next);
        
        computer_value = hand_value(&computer);

        //debug!("COMPUTER HIT: New: {}, Vec: {:?}, Value: {}", next, computer.clone(), computer_value);
    }

    let mut computer_hand = String::new();
    
    for card in computer.clone() {
        computer_hand.push_str(format!("{} ",card).as_str());
    }

    //debug!("FINAL COMPUTER: Hand: {}, Vec: {:?}, Value: {}", computer_hand, computer.clone(), computer_value);

    if computer_value > 21 {
        let message = format!("Dealer: {}\n\nYou: {}\n\nYou win!", 
            computer_hand, player_hand);
        //debug!("COMPUTER BUST: {}", message);
        match game.edit(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Blackjack")
                .description(message)
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await {
            Ok(_) => {},
            Err(e) => {
                error!("Blackjack Computer Bust error: {:?}", e);
            }
        };

        money_delta += (bet_amount as f32 * 2.0).round() as i64;
    }

    else if player_value == computer_value {
        let message = format!("Dealer: {}\n\nYou: {}\n\nIts a tie!", 
            computer_hand, player_hand);
        //debug!("TIE: {}", message);
        match game.edit(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Blackjack")
                .description(message)
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await {
            Ok(_) => {},
            Err(e) => {
                error!("Blackjack Tie error: {:?}", e);
            }
        }

        money_delta += bet_amount;
    }

    else if computer_value > player_value {
        let message = format!("Dealer: {}\n\nYou: {}\n\nYou lose!", 
            computer_hand, player_hand);
        //debug!("COMPUTER WINS: {}", message);
        match game.edit(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Blackjack")
                .description(message)
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await {
            Ok(_) => {},
            Err(e) => {
                error!("Blackjack Computer Wins error: {:?}", e);
            }
        };

        return Ok(());
    }
    else {
        let message = format!("Dealer: {}\n\nYou: {}\n\nYou win!", 
            computer_hand, player_hand);
        //debug!("PLAYER WINS");
        match game.edit(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Blackjack")
                .description(message)
                .colour(Colour::from_rgb(52, 152, 219))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            })
        }).await {
            Ok(_) => {},
            Err(e) => {
                error!("Blackjack Player Wins error: {:?}", e);
            }
        };

        money_delta += (bet_amount as f32 * 2.0).round() as i64;
    }

    match database.get_user(discord_id, guild_id).await? {
        Some(user) => {
            let mut update = user.update();
            update.nuggets = Some(user.nuggets + money_delta);

            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;
        },
        None => {
            error!("Blackjack second get user none.");
            return Ok(());
        }
    }

    Ok(())
}

fn hand_value(cards: &Vec<Card>) -> i8 {
    let mut total: i8 = 0;
    let mut ace_amount = 0;

    for card in cards {
        total += match card.value {
            CardValue::Two => 2,
            CardValue::Three => 3,
            CardValue::Four => 4,
            CardValue::Five => 5,
            CardValue::Six => 6,
            CardValue::Seven => 7,
            CardValue::Eight => 8,
            CardValue::Nine => 9,
            CardValue::Ten => 10,
            CardValue::Jack => 10,
            CardValue::Queen => 10,
            CardValue::King => 10,
            CardValue::Ace => {
                ace_amount += 1;
                0
            }
        }
    }

    if ace_amount > 0 {
        if total + 11 * ace_amount <= 21 {
            total += 11 * ace_amount
        }

        else {
            for i in (1..(ace_amount+1)).rev() {

                if total + 11 * i > 21 {

                    total += 1;

                    continue;
                }
                else {
                    total += 11;
                    break;
                }
            }
        }
    }

    total
}