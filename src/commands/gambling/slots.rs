use serenity::framework::standard::{macros::command, Args, CommandResult};

use serenity::model::prelude::*;
use serenity::prelude::*;
use serenity::utils::Colour;

use std::{fmt, thread, time};

use rand::prelude::*;

use crate::db::DbUser;

use diesel::SaveChangesDsl;

use std::collections::HashMap;

#[derive(Hash, Clone, Copy, Debug, PartialEq, Eq)]
enum Symbols {
    Tangerine,
    Lemon,
    Watermelon,
    Banana,
    Grapes,
    Cherries,
    Pineapple,
    Ring,
    Gem,
    BlackLargeSquare,
}

impl fmt::Display for Symbols {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Tangerine => ":tangerine:",
                Self::Lemon => ":lemon:",
                Self::Watermelon => ":watermelon:",
                Self::Banana => ":banana:",
                Self::Grapes => ":grapes:",
                Self::Cherries => ":cherries:",
                Self::Pineapple => ":pineapple:",
                Self::Ring => ":ring:",
                Self::Gem => ":gem:",
                Self::BlackLargeSquare => ":black_large_square:",
            }
        )
    }
}

impl Symbols {
    fn win(&self) -> i64 {
        match self {
            // TODO: adjust winning amounts
            Self::Tangerine => 200,
            Self::Lemon => 200,
            Self::Watermelon => 200,
            Self::Banana => 200,
            Self::Grapes => 200,
            Self::Cherries => 200,
            Self::Pineapple => 200,
            Self::Ring => 200,
            Self::Gem => 200,
            Self::BlackLargeSquare => -1, // will never happen
        }
    }
}

const SYMBOLS: [Symbols; 9] = [
    Symbols::Tangerine,
    Symbols::Lemon,
    Symbols::Watermelon,
    Symbols::Banana,
    Symbols::Grapes,
    Symbols::Cherries,
    Symbols::Pineapple,
    Symbols::Ring,
    Symbols::Gem,
];

#[command]
#[description("Play Slots!")]
#[only_in(guilds)]
async fn slots(ctx: &Context, msg: &Message, _args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data
        .get::<crate::DbContainer>()
        .expect("Couldn't retrieve DbContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let user = match database.get_user(discord_id, guild_id).await? {
        Some(u) => {
            // TODO: add cooldown checks
            u
        }
        None => {
            msg.channel_id
                .send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e.title("Slots")
                            .description("You don't have enough Nuggets! :gem:")
                            .colour(Colour::from_rgb(52, 152, 219))
                    })
                })
                .await?;

            return Ok(());
        }
    };

    if super::SLOT_PRICE > user.nuggets {
        msg.channel_id
            .send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e.title("Slots")
                        .description("You don't have enough Nuggets! :gem:")
                        .colour(Colour::from_rgb(52, 152, 219))
                })
            })
            .await?;

        return Ok(());
    }

    let mut update = user.update();

    update.nuggets = Some(user.nuggets - super::SLOT_PRICE);
    let _host: DbUser = update.save_changes(&*database.conn.lock().await)?;

    drop(user);

    // Game code start
    let mut game_setup: Vec<Vec<Symbols>> = Vec::new();
    let mut final_slot_machine_state: Vec<Vec<Symbols>> = Vec::new();

    let mut random = rand::rngs::OsRng;

    // fill the game setup with random symbols 5x15 vectors of symbols
    for _ in 0..5 {
        let mut temp: Vec<Symbols> = Vec::new();
        for _ in 0..15 {
            temp.push(SYMBOLS[random.gen_range(0..SYMBOLS.len())]);
        }
        game_setup.push(temp);
    }

    // Send the inital slot machine
    let mut message = msg
        .channel_id
        .send_message(&ctx.http, |m| {
            m.embed(|e| {
                e.title("Slots")
                    .description(format!("{}", progress_game(&game_setup, 0, &mut final_slot_machine_state)))
            })
        })
        .await?;

    // iterate 15 times to progress the game in slot_embed(...)
    for i in 0..15 {
        message
            .edit(&ctx.http, |m| {
                m.embed(|e| {
                    e.title("Test")
                        .description(format!("{}", progress_game(&game_setup, i, &mut final_slot_machine_state)))
                })
            })
            .await?;

        thread::sleep(time::Duration::from_millis(1500));
    }

    let mut solution_map: HashMap<Symbols, Vec<bool>> = HashMap::new();
    let temp = vec![false; 5];

    // for each symbol in the slot machine
    for i in 0..5 {
        for j in 0..5 {
            // get or insert that symbol from/into the solution map
            let val = solution_map.entry(final_slot_machine_state[j][i]).or_insert(temp.clone());

            // set its slot to true aka, this symbol was found in this slot
            val[i] = true;
        }
    }

    // stores the winning symbols if any
    let mut winners: Vec<Symbols> = Vec::new();

    // for each entry in the solution dict, if the val is true aka if all slots have had at least 1 of the symbol, add the symbol to winners
    for (key, value) in &solution_map {
        if value.iter().all(|&i| i) {
            winners.push(*key);
        }
    }

    // replace all symbols that are not winning with a black square
    if winners.len() != 0 {
        for i in 0..5 {
            for j in 0..5 {
                if !winners.contains(&final_slot_machine_state[i][j]) {
                    final_slot_machine_state[i][j] = Symbols::BlackLargeSquare;
                }
            }
        }

        let mut win_amount = 0;
        for symbol in winners {
            win_amount += symbol.win();
        }

        let user = match database.get_user(discord_id, guild_id).await? {
            Some(u) => u,
            _ => return Ok(()),
        };

        update.nuggets = Some(user.nuggets + win_amount);
        let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

        // TODO: please god make this pretty
        message
            .edit(&ctx.http, |m| {
                m.embed(|e| {
                    e.title(format!("You won {} :gem:", win_amount))
                        .description(format!("{}", get_slot_machine_representation(&final_slot_machine_state)))
                        .colour(Colour::from_rgb(15, 180, 15))
                })
            })
            .await?;
    } else {
        // TODO: please god make this pretty
        message
            .edit(&ctx.http, |m| {
                m.embed(|e| {
                    e.title("You Lost")
                        .description(format!("{}", get_slot_machine_representation(&final_slot_machine_state)))
                        .colour(Colour::from_rgb(180, 15, 15))
                })
            })
            .await?;
    }

    Ok(())
}

fn progress_game(game: &Vec<Vec<Symbols>>, count: usize, final_out: &mut Vec<Vec<Symbols>>) -> String {
    let mut slots_state: Vec<Vec<Symbols>> = Vec::new();

    // some weird math i did a few month ago to "roll" the correct slot down, idk, dont change it its fine i guess
    for i in 0..5_usize {
        let mut temp: Vec<Symbols> = Vec::new();

        if (count as i32) - (i as i32) > 9 {
            for j in 0..5_usize {
                temp.push(game[i][9 + j].clone());
            }
        } else if (count as i32) - (i as i32) < 0 {
            for j in 0..5_usize {
                temp.push(game[i][j].clone());
            }
        } else {
            for j in 0..5_usize {
                temp.push(game[i][count - i + j].clone());
            }
        }

        slots_state.push(temp.clone());
    }

    *final_out = vec![vec![Symbols::BlackLargeSquare; 5]; 5];
    for i in 0..5_usize {
        for j in 0..5_usize {
            final_out[j][i] = slots_state[i][4 - j].clone();
        }
    }

    get_slot_machine_representation(&final_out)
}

fn get_slot_machine_representation(slots: &Vec<Vec<Symbols>>) -> String {
    slots
        .iter()
        .map(|row| {
            row.iter()
                .map(|e| e.to_string())
                .collect::<Vec<String>>()
                .join("┃")
        })
        .collect::<Vec<String>>()
        .join("\n")
}
