use serenity::{

    framework::standard::{
        CommandResult, macros::command
    },

    client::bridge::gateway::ShardId,
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use chrono::prelude::*;

#[command]
#[description("Pings the bot")]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let shard_manager = data.get::<crate::ShardManagerContainer>().expect("Couldn't retrieve ShardManagerContainer");

    let lock = shard_manager.lock().await;
    let shard_runners = lock.runners.lock().await;

    let runner = shard_runners.get(&ShardId(ctx.shard_id)).unwrap();

    let latency = match runner.latency { Some(l) => format!("{}ms", l.as_millis()) , None => "N/A".to_owned() };

    let before = Utc::now();
    let mut message = msg.channel_id.say(&ctx.http, "Ping!").await?;
    let after = Utc::now();

    let duration = after - before;

    message.edit(&ctx.http, |m| {
        m.content(format!("Pong!\n\nLatency `{}ms`\nShard ID `{}/{}` Latency `{}`",
        duration.num_milliseconds(), ctx.shard_id, shard_runners.len(), latency))
    }).await?;
    Ok(())
}