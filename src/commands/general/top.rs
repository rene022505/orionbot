use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

#[command]
#[only_in(guilds)]
#[description("View leaderboard!")]
async fn top(ctx: &Context, msg: &Message) -> CommandResult {
    let guild = match msg.guild_id.unwrap().to_guild_cached(&ctx).await {
        Some(guild) => guild,
        None => {
            error!("Could not find guild {} in cache", *msg.guild_id.unwrap().as_u64());
            return Ok(());
        }
    };

    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let top_users = database.select_top_10_users(*msg.guild_id.unwrap().as_u64()).await?;

    let mut fields = Vec::new();

    for (i, top_user) in top_users.iter().enumerate() {
        let id = UserId(top_user.discord_id.0);

        let tag = match guild.members.get(&id) {
            Some(member) => {
                member.user.tag()
            }
            None => {
                match id.to_user(&ctx).await {
                    Ok(u) => u.tag(),
                    Err(_e) => {
                        //warn!("Could not get user {} from REST API or guild cache: {}", top_user.discord_id.0, e);
                        continue;
                    }
                }
            }
        };

        fields.push((format!("{}. {}", i+1, tag), format!("Nuggets: {} :gem:\n",
            top_user.nuggets)));
    }

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Leaderboard")
            .description("Top 10")
            .colour(Colour::from_rgb(52, 152, 219));

            for field in fields {
                e.field(field.0, field.1, false);
            }

            e
        })
    }).await?;

    Ok(())
}
