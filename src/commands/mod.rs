mod general;
pub use general::*;
mod economy;
pub use economy::*;
mod gambling;
pub use gambling::*;
mod config;
pub use config::*;
mod help;
pub use help::*;

use serenity::framework::standard::{
        CommandResult, macros::hook,
    };
use serenity::prelude::*;
use serenity::model::prelude::*;

use crate::cache::*;

pub const IGNORE_WHITELIST: &'static[&'static str] = &[
    "config"
];

#[hook]
pub async fn after(ctx: &Context, msg: &Message, command_name: &str, command_result: CommandResult) {
    match command_result {
        Ok(_) => (),
        Err(e) => {
            error!("Command {} -- {:?}", command_name, e);
            return;
        },
    }

    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");
    let bot_cache = data.get::<BotCacheContainer>().expect("Couldn't retrieve BotCacheContainer");

    let (guild_id, guild_id_u64) = match msg.guild_id {
        Some(g_id) => (g_id, *g_id.as_u64()),
        None => return,
    };

    let db_guild = match database.get_guild(guild_id_u64).await {
        Ok(optn_guild) => match optn_guild {
            Some(g) => g,
            None => return,
        }
        Err(e) => {
            error!("Database error get guild before command: {:?}", e);
            return;
        }
    };

    if let Some(richest_role) = db_guild.richest_role {
        let nuggets = match database.select_top_10_users(guild_id_u64).await {
            Ok(users) => users,
            Err(e) => {
                error!("Database error select top 10 users before command: {:?}", e);
                return;
            }
        };

        if let Some(u) = nuggets.first() {
            if let Some(old) = bot_cache.lock().await.richest.insert(guild_id_u64, u.discord_id.0) {
                if old != u.discord_id.0 {
                    if let Ok(mut member) = guild_id.member(&ctx.http, &UserId(old)).await {      
                        if let Err(e) = member.remove_role(&ctx.http, richest_role.0).await {
                            error!("Failed to remove Richest role for {}: {:?}", old, e);
                        }
                    }
                }   
            }

            if let Ok(mut member) = guild_id.member(&ctx.http, &UserId(u.discord_id.0)).await {       
                if let Err(e) = member.add_role(&ctx.http, richest_role.0).await {
                    error!("Failed to add Richest role for {}: {:?}", u.discord_id.0, e);
                }
            }
        }
    }

    if let Some(longest_spin_role) = db_guild.longest_spin_role {
        let spinners = match database.select_top_10_spinners(guild_id_u64).await {
            Ok(users) => users,
            Err(e) => {
                error!("Database error select top 10 spinners before command: {:?}", e);
                return;
            }
        };

        if let Some(u) = spinners.first() {
            if let Some(old) = bot_cache.lock().await.longest_spin.insert(guild_id_u64, u.discord_id.0) {
                if old != u.discord_id.0 {
                    if let Ok(mut member) = guild_id.member(&ctx.http, &UserId(old)).await {
                        if let Err(e) = member.remove_role(&ctx.http, longest_spin_role.0).await {
                            error!("Failed to remove Longest Spin role for {}: {:?}", old, e);
                        }
                    }   
                }         
            }

            if let Ok(mut member) = guild_id.member(&ctx.http, &UserId(u.discord_id.0)).await {             
                if let Err(e) = member.add_role(&ctx.http, longest_spin_role.0).await {
                    error!("Failed to add Longest Spin role for {}: {:?}", u.discord_id.0, e);
                }
            }
        }
    }
}

#[hook]
pub async fn before(ctx: &Context, msg: &Message, command_name: &str) -> bool {

    let data = ctx.data.read().await;
    let database = data.get::<DbContainer>().expect("Couldn't retrieve DbContainer");

    let is_enabled = match database.is_command_enabled(*msg.guild_id.unwrap().as_u64(), *msg.channel_id.as_u64(), command_name).await {
        Ok(b) => b,
        Err(e) => {
            error!("Before hook, database error: {:?}", e);
            false
        }
    };

    is_enabled
}