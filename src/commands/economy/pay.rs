use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use crate::db::DbUser;

use nebbot_utils::{
    discord::{ MatchDiscordUser, match_discord_user },
};

use diesel::SaveChangesDsl;

#[command]
#[description("Pay another user")]
#[usage("!pay <user> <amount>")]
#[only_in(guilds)]
async fn pay(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let user = match match_discord_user(&args, &ctx, discord_id).await {
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Pay")
                    .description("You cannot pay a bot!")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::SameAsHost => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Pay")
                    .description("You cannot pay yourself!")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::InvalidForm(_) | MatchDiscordUser::NoArgument |
        MatchDiscordUser::NoUserFound => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Pay")
                    .description("Invalid User Argument")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
    };

    let amount = match args.advance().current() {
        Some(s) => {
            match s.parse::<i64>() {
                Ok(i) => i,
                Err(_) => {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Pay")
                            .description("Invalid amount")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
        
                    return Ok(());
                }
            }
        },

        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Pay")
                    .description("Invalid amount")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        }
    };

    if amount < 0 {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Pay")
                .description("Nice try, thief!")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    if amount == 0 {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Pay")
                .description("Invalid amount")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    let host = match database.get_user(discord_id, guild_id).await? {
        Some(u) => u,
        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Pay")
                    .description("You don't have enough Nuggets! :gem:")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(())
        }
    };

    if host.nuggets < amount {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Pay")
                .description("You don't have enough Nuggets! :gem:")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    }

    match database.get_user(*user.id.as_u64(), guild_id).await? {
        Some(other) => {
            let mut update = other.update();
            update.nuggets = Some(other.nuggets + amount);
            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;
        },
        None => {
            let mut default = DbUser::default(discord_id, guild_id);

            default.nuggets += amount;

            database.add_user(default).await?;
        }
    }

    let mut update = host.update();
    update.nuggets = Some(host.nuggets - amount);
    let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Pay")
            .description(format!("Successfully paid {} Nuggets! :gem: to {}", amount, user.mention()))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    Ok(())
}