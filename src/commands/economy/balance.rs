use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use nebbot_utils::discord::{ match_discord_user, MatchDiscordUser };

#[command]
#[aliases("bal", "b", "nuggets", "n")]
#[description("Check your balance")]
#[only_in(guilds)]
async fn balance(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let dc_user = match match_discord_user(&args, &ctx, 0).await {
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) | MatchDiscordUser::NoUserFound |
        MatchDiscordUser::InvalidForm(_) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Balance")
                    .description("Invalid User Argument")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::SameAsHost | 
        MatchDiscordUser::NoArgument => msg.author.clone(),
    };

    let id = *dc_user.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let user = database.get_user_or_default(id, guild_id).await?;

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Balance")
            .description(format!("{} has {} Nuggets! :gem:", dc_user.mention(), user.nuggets))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    Ok(())
}