use serenity::framework::standard::{
    CommandResult, Args, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use crate::db::{ DbUser };

use nebbot_utils::{
    discord::{ MatchDiscordUser, match_discord_user },
};

use diesel::SaveChangesDsl;

#[command]
#[description("Give another user Nuggets! :gem:")]
#[usage("!give <user> <amount>")]
#[only_in(guilds)]
#[owners_only]
async fn give(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let user = match match_discord_user(&args, &ctx, discord_id).await {
        MatchDiscordUser::SameAsHost => msg.author.clone(), 
        MatchDiscordUser::Other(u) => u,
        MatchDiscordUser::Bot(_) => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Give")
                    .description("You cannot give Nuggets! :gem: to a bot!")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
        MatchDiscordUser::InvalidForm(_) | MatchDiscordUser::NoArgument |
        MatchDiscordUser::NoUserFound => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Give")
                    .description("Invalid User Argument")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        },
    };

    let amount = match args.advance().current() {
        Some(s) => {
            match s.parse::<i64>() {
                Ok(i) => i,
                Err(_) => {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Give")
                            .description("Invalid amount")
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
        
                    return Ok(());
                }
            }
        },

        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Give")
                    .description("Invalid amount")
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        }
    };

    if amount <= 0 {
        msg.channel_id.send_message(&ctx.http, |m| {
            m.embed(|e| {
                e
                .title("Give")
                .description("Amount must be greater than 0.")
                .colour(Colour::from_rgb(52, 152, 219))
            })
        }).await?;

        return Ok(());
    } 
   
    match database.get_user(user.id.0, guild_id).await? {
        Some(other) => {
            let mut update = other.update();

            update.nuggets = Some(other.nuggets + amount);
            let _: DbUser = update.save_changes(&*database.conn.lock().await)?;
        },
        None => {
            let mut default = DbUser::default(discord_id, guild_id);

            default.nuggets += amount;

            database.add_user(default.clone()).await?;
        }
    }

    msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            e
            .title("Give")
            .description(format!("Successfully given {} Nuggets! :gem: to {}", amount, user.mention()))
            .colour(Colour::from_rgb(52, 152, 219))
        })
    }).await?;

    Ok(())
}
