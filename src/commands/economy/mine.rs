use serenity::framework::standard::{
    CommandResult, macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::utils::Colour;

use chrono::prelude::*;
use chrono::Duration;

use rand::prelude::*;

use crate::db::DbUser;

use nebbot_utils::time::human_readable;

use diesel::SaveChangesDsl;

#[command]
#[description("Mine some nuggets every 4 hours!")]
#[only_in(guilds)]
async fn mine(ctx: &Context, msg: &Message) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mut random = rand::rngs::OsRng;

    let discord_id = *msg.author.id.as_u64();
    let guild_id = *msg.guild_id.unwrap().as_u64();

    let mut delta: i64 = 0;
    let gained;

    match database.get_user(discord_id, guild_id).await? {
        Some(user) => {
            let duration = Utc::now() - user.last_mine;

            if duration <= Duration::hours(super::MINE_COOLDOWN) {

                let time_next = Duration::hours(super::MINE_COOLDOWN) - duration;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You need to wait {}",
                            human_readable(time_next)))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                return Ok(());
            }

            if !user.has_pickaxe {
                if user.nuggets < super::PICKAXE_PRICE {
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Mine")
                            .description(
                                format!("You need a pickaxe, which costs {} Nuggets! :gem: but you are too poor to afford it!\n\nThere is also an additional fee of {} Nuggets! :gem: per mine.\n\nCome back when you have {} Nuggets! :gem:",
                                    super::PICKAXE_PRICE, super::MINE_COST, super::PICKAXE_PRICE + super::MINE_COST))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;

                    return Ok(());
                }
                
                let mut message = msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You need a pickaxe. Would you like to buy one for {} Nuggets! :gem: ?\n\nThere is also an additional fee of {} Nuggets! :gem: per mine.", super::PICKAXE_PRICE, super::MINE_COST))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;

                message.react(&ctx.http, ReactionType::Unicode("✅".to_string())).await?;
                message.react(&ctx.http, ReactionType::Unicode("❌".to_string())).await?;

                match message.await_reaction(&ctx).timeout(std::time::Duration::from_secs(30)).author_id(discord_id)
                    .filter(|reaction| match reaction.emoji.as_data().as_str() {
                        "✅" | "❌" => true,
                        _ => false,
                    }).await 
                {
                    Some(reaction) => {
                        let emoji = &reaction.as_inner_ref().emoji;

                        match emoji.as_data().as_str() {
                            "✅" => {
                                delta -= super::PICKAXE_PRICE;
                            }
                            "❌" => {
                                message.edit(&ctx.http, |m| {
                                    m
                                    .embed(|e| {
                                        e
                                        .title("Mine")
                                        .description(format!("~~You need a pickaxe. Would you like to buy one for {} Nuggets! :gem: ?\n\nThere is also an additional fee of {} Nuggets! :gem: per mine.~~\n\nCancelled.", super::PICKAXE_PRICE, super::MINE_COST))
                                        .colour(Colour::from_rgb(52, 152, 219))
                                    })
                                }).await?;

                                return Ok(());
                            }

                            _ => {}
                        }
                    }

                    None => {
                        message.edit(&ctx.http, |m| {
                            m
                            .embed(|e| {
                                e
                                .title("Mine")
                                .description(format!("~~You need a pickaxe. Would you like to buy one for {} Nuggets! :gem: ?\n\nThere is also an additional fee of {} Nuggets! :gem: per mine.~~\n\nNo response.", super::PICKAXE_PRICE, super::MINE_COST))
                                .colour(Colour::from_rgb(52, 152, 219))
                            })
                        }).await?;

                        return Ok(());
                    }
                }
            }

            if user.nuggets + delta < super::MINE_COST {
                let mut update = user.update();

                update.nuggets = Some(user.nuggets + delta);
                update.has_pickaxe = Some(true);

                let _:DbUser = update.save_changes(&*database.conn.lock().await)?;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("You need to pay the entrance fee, but you are too poor to afford it. Come back when you have {} Nuggets! :gem:", super::MINE_COST))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;
            }
            else {
                delta -= super::MINE_COST;

                let bracket: f32 = random.gen();

                // 1% chance
                if bracket <= 0.01 {
                    let mut update = user.update();

                    update.nuggets = Some(user.nuggets + delta);
                    update.has_pickaxe = Some(false);
                    update.last_mine = Some(Utc::now());
    
                    let _: DbUser = update.save_changes(&*database.conn.lock().await)?;
    
                    msg.channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| {
                            e
                            .title("Mine")
                            .description(format!("Entrance fee: -{} Nuggets! :gem: \n\nYour pickaxe broke!", super::MINE_COST))
                            .colour(Colour::from_rgb(52, 152, 219))
                        })
                    }).await?;
    
                    return Ok(());
                }
    
                // 3% chance
                else if bracket <= 0.04 {
                    gained = random.gen_range(40..150);
                }
    
                // 50% chance
                else if bracket <= 0.54 {
                    gained = random.gen_range(150..300);
                }
    
                // 33% chance
                else if bracket <= 0.87 {
                    gained = random.gen_range(300..500);
                }

                // 10% chance
                else if bracket <= 0.97 {
                    gained = random.gen_range(500..800);
                }

                // 3% chance
                else {
                    gained = random.gen_range(800..2000);
                }

                delta += gained;

                let mut update = user.update();

                update.nuggets = Some(user.nuggets + delta);
                update.has_pickaxe = Some(true);
                update.last_mine = Some(Utc::now());

                let _: DbUser = update.save_changes(&*database.conn.lock().await)?;

                msg.channel_id.send_message(&ctx.http, |m| {
                    m.embed(|e| {
                        e
                        .title("Mine")
                        .description(format!("Entrance fee: -{} Nuggets! :gem: \n\nYou mined {} Nuggets! :gem: You now have {} Nuggets! :gem:",
                            super::MINE_COST, gained, user.nuggets + delta))
                        .colour(Colour::from_rgb(52, 152, 219))
                    })
                }).await?;
            }
        }

        None => {
            msg.channel_id.send_message(&ctx.http, |m| {
                m.embed(|e| {
                    e
                    .title("Mine")
                    .description(format!("You need a pickaxe, but you are too poor to afford it. Come back when you have {} Nuggets! :gem:", super::PICKAXE_PRICE))
                    .colour(Colour::from_rgb(52, 152, 219))
                })
            }).await?;

            return Ok(());
        }
    }

    Ok(())
}